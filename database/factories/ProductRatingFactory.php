<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\Database\Factories;

use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\ProductRatings\Models\ProductRating;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<ProductRating>
 */
final class ProductRatingFactory extends Factory
{
    /** @var class-string<ProductRating> $model */
    public $model = ProductRating::class;
    /**
     * @throws Exception
     */
    public function definition(): array
    {
        return [
            'product_id' => (new ProductFactory())->createOne(),
            'client_id' => (new ClientFactory())->createOne(),
            'rating' => random_int(1, 5),
            'title' => $this->faker->name(),
            'message' => $this->faker->text(1000),
            'active' => 1,
        ];
    }
}
