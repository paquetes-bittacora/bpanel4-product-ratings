<?php

declare(strict_types=1);

return [
    'edit' => 'Editar',
    'title' => 'Título',
    'message' => 'Mensaje',
    'rating' => 'Valoración',
    'client' => 'Cliente',
    'product' => 'Producto',
    'active' => 'Mostrada',
    'rating-updated' => 'Valoración actualizada',
    'rating-not-updated' => 'Ocurrió un error al actualizar la valoración',
];
