<?php

return [
    'must-be-logged-in' => 'Debe iniciar sesión para publicar una valoración',
    'no-ratings-yet' => 'Todavía no se ha publicado ninguna valoración para este producto',
];
