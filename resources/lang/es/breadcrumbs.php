<?php

declare(strict_types=1);

return [
    'bpanel4-product-ratings' => 'Valoraciones de productos',
    'index' => 'Listar',
    'edit' => 'Editar',
];
