<div class="d-flex star-rating-summary" title="{{ $average }}">
    @include('bpanel4-product-ratings::partials.star-rating', ['value' => $average])
    ({{ $count }} opiniones)
</div>