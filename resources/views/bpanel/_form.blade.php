<?php /** @var \Bittacora\Bpanel4\ProductRatings\Models\ProductRating|null $rating */ ?>
<form class="mt-lg-3" autocomplete="off" method="post" action="{{ $action }}" enctype="multipart/form-data">
    @csrf
    <div class="form-group form-row">
        <div class="col-sm-3 col-form-label text-sm-right">
            <label class="mb-0  ">
                {{ __('bpanel4-product-ratings::general.client') }}
            </label>
        </div>
        <div class="col-sm-7 d-flex align-items-center">
            <div class="input-group m-b">
                <a href="{{ route('bpanel4-clients.edit', ['client' => $rating->client_id]) }}" target="_blank">{{ $rating->getClient()->getFullName() }} <i class="fas fa-external-link-alt"></i></a>
            </div>
        </div>
    </div>
    <div class="form-group form-row">
        <div class="col-sm-3 col-form-label text-sm-right">
            <label class="mb-0  ">
                {{ __('bpanel4-product-ratings::general.product') }}
            </label>
        </div>
        <div class="col-sm-7 d-flex align-items-center">
            <div class="input-group m-b">
                <a href="{{ route('bpanel4-products.edit', ['model' => $rating->product_id]) }}" target="_blank">{{ $rating->getProduct()->getName() }} <i class="fas fa-external-link-alt"></i></a>
            </div>
        </div>
    </div>
    <div class="form-group form-row">
        <div class="col-sm-3 col-form-label text-sm-right">
            <label class="mb-0  ">
                {{ __('bpanel4-product-ratings::general.rating') }}
            </label>
        </div>
        <div class="col-sm-7 d-flex align-items-center">
            <div class="input-group m-b">
                @include('bpanel4-product-ratings::partials.star-rating', ['value' => $rating['rating']])
            </div>
        </div>
    </div>
    @livewire('form::input-text', ['name' => 'title', 'labelText' => __('bpanel4-product-ratings::general.title'),
    'required'=>true, 'value' => old('title') ?? $rating?->getTitle() ])
    @livewire('form::textarea', ['name' => 'message', 'labelText' =>
    __('bpanel4-product-ratings::general.message'), 'value' => old('message') ??
    $rating?->getMessage() ])
    @livewire('form::input-checkbox', ['name' => 'active', 'value' => 1, 'checked' => old('active') ?? $rating?->isActive(), 'labelText' => __('bpanel4-product-ratings::general.active'), 'bpanelForm' => true])
    <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
        @livewire('form::save-button',['theme'=>'update'])
        @livewire('form::save-button',['theme'=>'reset'])
    </div>
    @if($rating?->getId() !== null)
        <input type="hidden" name="id" value="{{ $rating->getId() }}">
    @endif

    @if(isset($language))
        <input type="hidden" name="locale" value="{{ $language }}">
    @endif
    <style>
        .star-rating .stars {
            display: flex;
            gap: 0.5rem;
            align-items: flex-end;
            line-height: 1em;
            color: #ffb433;
        }
    </style>
</form>
