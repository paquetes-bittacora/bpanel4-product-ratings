@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Editar opinión')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-product-ratings::general.edit') }}</span>
            </h4>
        </div>
        <div class="card-body">
            @include('bpanel4-product-ratings::bpanel._form')
        </div>
    </div>


@endsection
