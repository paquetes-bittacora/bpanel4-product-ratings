<?php /** @var \Bittacora\Bpanel4\ProductRatings\Models\ProductRating $row */ ?>
<td>
    <a href="{{ route('bpanel4-clients.edit', ['client' => $row->client_id]) }}" target="_blank">{{ $row->getClient()->getFullName() }} <i class="fas fa-external-link-alt"></i></a>
</td>
<td>
    <a href="{{ route('bpanel4-products.edit', ['model' => $row->product_id]) }}" target="_blank">{{ $row->getProduct()->getName() }} <i class="fas fa-external-link-alt"></i></a>
</td>
<td class="text-center">
    @if($row->rating >= 4)
        <span class="badge badge-pill badge-success">{{ $row->rating }}</span>
    @elseif($row->rating >= 3)
        <span class="badge badge-pill badge-warning">{{ $row->rating }}</span>
    @else
        <span class="badge badge-pill badge-danger">{{ $row->rating }}</span>
    @endif
</td>
<td>
    {{ substr($row->message, 0, 60) }} @if(strlen($row->message) > 60)...@endif
</td>
<td>
    {{ substr($row->message, 0, 60) }} @if(strlen($row->message) > 60)...@endif
</td>
<td class="align-middle">
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-user-'.$row->id))
</td>
<td class="align-middle">
    <div class="text-center">
        @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'bpanel4-product-ratings', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'la valoración?'], key('rating-buttons-'.$row->id))
    </div>
</td>
