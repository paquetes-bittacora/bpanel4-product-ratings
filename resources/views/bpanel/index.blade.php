@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Opiniones de productos')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-product-ratings::datatable.index') }}</span>
            </h4>
        </div>
        <div class="card-body">
            @livewire('bpanel4-product-ratings::livewire.ratings-table', [], key('bpanel4-product-ratings-datatable'))
        </div>
    </div>


@endsection
