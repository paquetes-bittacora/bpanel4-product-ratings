<!--suppress HtmlFormInputWithoutLabel -->
@auth
    <form class="product-rating-form" method="post" action="{{ route('bpanel4-product-ratings.store') }}">
        @csrf
        <div class="form-group mb-4 title">
            <input type="text" class="form-control" id="title" placeholder="Título de su opinión" name="title">
        </div>
        <div class="form-group mb-4 message">
            <textarea class="form-control" placeholder="Escriba aquí su opinión sobre el producto." name="text"></textarea>
        </div>
        <div class="form-group d-flex align-items-center rating-row mb-4">
            <div class="pr-3">Valoración del producto</div>
            @include('bpanel4-product-ratings::partials.star-rating', ['value' => 5, 'fieldId' => 'rating'])
        </div>
        <input type="hidden" name="product_id" value="{{ Crypt::encryptString($product->getId()) }}">
        <button type="submit" class="btn btn-primary">Enviar</button>
        @if($errors->any())
            {!! implode('', $errors->all('<div>:message</div>')) !!}
        @endif
    </form>
@elseguest
    <div class="alert alert-warning product-ratings-warning">
        <i class="fas fa-exclamation-triangle"></i> {{ __('bpanel4-product-ratings::public.must-be-logged-in') }}
    </div>
@endauth