<div>
    @include('bpanel4-product-ratings::livewire._form', ['product' => $product])
    <div class="ratings">
        @forelse($ratings as $rating)
            <div class="rating">
                <div class="title">
                    <div>{{ $rating['title'] }}</div>
                    <div class="date">{{ date_format(new DateTime($rating['created_at']), 'd\/m\/y') }}</div>
                </div>
                <div class="client-name">
                    {{ $rating['client']['name'] }} {{ $rating['client']['surname'] }}
                    <div class="client-stars">
                        @include('bpanel4-product-ratings::partials.star-rating', ['value' => $rating['rating']])
                    </div>
                </div>
                <div class="message">
                    {{ $rating['message'] }}
                </div>
            </div>
        @empty
            <p class="no-ratings-yet">{{ __('bpanel4-product-ratings::public.no-ratings-yet') }}</p>
        @endforelse
        <div class="ratings-pagination">
            {{ $ratings->links() }}
        </div>
    </div>
</div>