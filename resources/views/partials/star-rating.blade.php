<div class="star-rating" @if(isset($fieldId)) id="{{ $fieldId }}" data-enabled="true" @else data-enabled="false"
     @endif data-selected-value="{{ $value }}"  data-current-value="">
    <div class="stars" >
        @for($i=0; $i<5; $i++)
            <div class="star">
                @if($value - $i >= 1)
                    <i class="fas fa-star"></i>
                @elseif($value - $i >= .5)
                    <i class="fad fa-star-half"></i>
                @else
                    <i class="far fa-star"></i>
                @endif
            </div>
        @endfor
    </div>
    @if(isset($fieldId))
        <input type="hidden" name="{{ $fieldId }}" value="" class="rating-input">
    @endif
</div>