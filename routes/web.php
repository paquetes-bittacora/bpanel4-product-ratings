<?php

declare(strict_types=1);

use Bittacora\Bpanel4\ProductRatings\Http\Controllers\ProductRatingController;
use Bittacora\Bpanel4\ProductRatings\Http\Controllers\ProductRatingsAdminController;
use Illuminate\Support\Facades\Route;

Route::prefix('/opiniones-de-productos')->name('bpanel4-product-ratings.')->middleware(['web'])
    ->group(static function (): void {
        Route::post('/store', [ProductRatingController::class, 'store'])->name('store');
    });

Route::prefix('bpanel/opiniones-de-productos')->name('bpanel4-product-ratings.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(static function (): void {
        Route::get('/listar', [ProductRatingsAdminController::class, 'index'])->name('index');
        Route::get('/{rating}/editar', [ProductRatingsAdminController::class, 'edit'])->name('edit');
        Route::post('/actualizar', [ProductRatingsAdminController::class, 'update'])->name('update');
        Route::delete('/eliminar', [ProductRatingsAdminController::class, 'destroy'])->name('destroy');
    });
