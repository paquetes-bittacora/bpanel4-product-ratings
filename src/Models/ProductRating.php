<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\Models;

use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Products\Models\Product;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Bittacora\Bpanel4\ProductRatings\Models\ProductRating
 *
 * @property int $id
 * @property int $product_id
 * @property int $client_id
 * @property float $rating
 * @property string $title
 * @property string $message
 * @property int|bool $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|ProductRating newModelQuery()
 * @method static Builder|ProductRating newQuery()
 * @method static Builder|ProductRating query()
 * @method static Builder|ProductRating whereActive($value)
 * @method static Builder|ProductRating whereClientId($value)
 * @method static Builder|ProductRating whereCreatedAt($value)
 * @method static Builder|ProductRating whereId($value)
 * @method static Builder|ProductRating whereMessage($value)
 * @method static Builder|ProductRating whereProductId($value)
 * @method static Builder|ProductRating whereRating($value)
 * @method static Builder|ProductRating whereTitle($value)
 * @method static Builder|ProductRating whereUpdatedAt($value)
 * @method static Builder|ProductRating where($column, $value)
 * @mixin Eloquent
 */
final class ProductRating extends Model
{
    public $table = 'products_ratings';

    public $casts = [
        'active' => 'boolean',
    ];

    public function getId(): int
    {
        return $this->id;
    }

    public function setProductId(int $productId): void
    {
        $this->product_id = $productId;
    }

    public function setClientId(int $clientId): void
    {
        $this->client_id = $clientId;
    }

    public function getRating(): float
    {
        return $this->rating;
    }

    public function setRating(float $rating): void
    {
        $this->rating = $rating;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function isActive(): bool
    {
        return (bool) $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return BelongsTo<Client, ProductRating>
     */
    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function getClient(): Client
    {
        return $this->client()->firstOrFail();
    }

    public function getProduct(): Product
    {
        return $this->product()->firstOrFail();
    }
}
