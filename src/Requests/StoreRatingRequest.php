<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\Requests;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Encryption\Encrypter;
use Illuminate\Foundation\Http\FormRequest;
use Webmozart\Assert\Assert;

/**
 * @method array<string, string|float> validated($key = null, $default = null)
 */
final class StoreRatingRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array<string, string>
     */
    public function rules(): array
    {
        return [
            'title' => 'string|required',
            'text' => 'string|nullable',
            'rating' => 'numeric|required|min:0|max:5',
            'product_id' => 'required|numeric',
        ];
    }

    /**
     * @throws BindingResolutionException
     */
    public function getValidatorInstance(): Validator
    {
        $crypt = $this->container->make(Encrypter::class);
        $productId = $this->get('product_id');
        Assert::string($productId);
        $this->merge(['product_id' => $crypt->decrypt($productId, false)]);
        return parent::getValidatorInstance();
    }
}
