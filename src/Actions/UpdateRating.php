<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\Actions;

use Bittacora\Bpanel4\ProductRatings\Dto\RatingDto;
use Bittacora\Bpanel4\ProductRatings\Models\ProductRating;

final class UpdateRating
{
    public function execute(RatingDto $dto): void
    {
        $rating = ProductRating::whereId($dto->id)->firstOrFail();

        $rating->setTitle($dto->title);
        $rating->setMessage($dto->message);
        $rating->setActive($dto->active);

        $rating->save();
    }
}
