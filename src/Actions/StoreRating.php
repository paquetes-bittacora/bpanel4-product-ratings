<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\Actions;

use Bittacora\Bpanel4\ProductRatings\Models\ProductRating;
use Webmozart\Assert\Assert;

final class StoreRating
{
    /**
     * @param array<string, string|float|null|int> $data
     */
    public function execute(array $data): void
    {
        $this->validateInputData($data);
        $productRating = new ProductRating();
        $productRating->setRating($this->roundRating((float) $data['rating']));
        $productRating->setActive(false); // Por defecto las opiniones no están aprobadas
        $productRating->setTitle($data['title']);
        $productRating->setMessage($data['text'] ?? '');
        $productRating->setClientId((int) $data['client_id']);
        $productRating->setProductId((int) $data['product_id']);
        $productRating->save();
    }

    /**
     * @param array<string, string|float|null|int> $data
     * @phpstan-assert array{
     *      client_id: int|numeric-string,
     *      product_id: int|numeric-string,
     *      rating: float|numeric-string,
     *      title: string,
     *      text: string|null
     * } $data
     */
    private function validateInputData(array $data): void
    {
        Assert::string($data['title']);
        Assert::nullOrString($data['text']);
        Assert::numeric($data['rating']);
        Assert::numeric($data['client_id']);
        Assert::numeric($data['product_id']);
    }

    public function roundRating(float $rating): float
    {
        $intPart = (int)$rating;
        return ($rating - $intPart) >= .5 ? $intPart + .5 : $intPart;
    }
}
