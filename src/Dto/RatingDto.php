<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\Dto;

use Bittacora\Dtos\Dto;

final class RatingDto extends Dto
{
    public function __construct(
        public readonly int $id,
        public readonly string $title,
        public readonly string $message,
        public readonly bool $active,
    ) {
    }
}
