<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\View\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

final class SingleRating extends Component
{
    public function __construct(
        public readonly float $rating,
    ) {
    }

    public function render(): View
    {
        return $this->view('bpanel4-product-ratings::components.single-rating');
    }
}
