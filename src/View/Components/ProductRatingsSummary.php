<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\View\Components;

use Bittacora\Bpanel4\ProductRatings\Services\ProductRatingsService;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

final class ProductRatingsSummary extends Component
{
    public function __construct(
        protected readonly int $productId,
        private readonly ProductRatingsService $productRatingsService
    ) {
    }

    public function render(): View
    {
        return $this->view('bpanel4-product-ratings::components.product-ratings-summary', [
            'average' => round($this->productRatingsService->getAverage($this->productId), 1),
            'count' => $this->productRatingsService->getCount($this->productId),
        ]);
    }
}
