<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\Services;

use Bittacora\Bpanel4\ProductRatings\Models\ProductRating;
use RuntimeException;

final class ProductRatingsService
{
    public function getAverage(int $productId): float
    {
        $average = ProductRating::where('product_id', $productId)->avg('rating');

        if (null === $average) {
            return 5.0;
        }

        if (!is_float($average)) {
            throw new RuntimeException('Error al calcular la valoración media para el producto');
        }

        return $average;
    }

    public function getCount(int $productId): int
    {
        return ProductRating::where('product_id', $productId)->count();
    }
}
