<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Bittacora\Tabs\Tabs;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    /** @var string $signature */
    public $signature = 'bpanel4-product-ratings:install';

    private const PERMISSIONS = [
        'index',
        'create',
        'show',
        'edit',
        'destroy',
        'store',
        'update',
    ];

    public function handle(AdminMenu $adminMenu): void
    {
        $this->createMenuEntries($adminMenu);
        $this->createTabs();
        $this->giveAdminPermissions();
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'bpanel4-product-ratings.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }

    private function createTabs(): void
    {
        Tabs::createItem(
            'bpanel4-product-ratings.index',
            'bpanel4-product-ratings.index',
            'bpanel4-product-ratings.index',
            'Opiniones de productos',
            'fa fa-list'
        );
    }

    private function createMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo al menú de administración...');

        $adminMenu->createGroup('ecommerce', 'Tienda', 'far fa-shopping-cart');
        $adminMenu->createModule('ecommerce', 'bpanel4-product-ratings', 'Opiniones', 'fas fa-star');
        $adminMenu->createAction('bpanel4-product-ratings', 'Listado', 'index', 'fas fa-list');
    }
}
