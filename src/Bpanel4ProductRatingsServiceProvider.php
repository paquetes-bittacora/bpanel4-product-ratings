<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings;

use Bittacora\Bpanel4\ProductRatings\Commands\InstallCommand;
use Bittacora\Bpanel4\ProductRatings\Http\Livewire\ProductRatingsDatatable;
use Bittacora\Bpanel4\ProductRatings\Http\Livewire\ProductRatingsList;
use Bittacora\Bpanel4\ProductRatings\View\Components\ProductRatingsSummary;
use Bittacora\Bpanel4\ProductRatings\View\Components\SingleRating;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;
use Livewire\LivewireManager;

final class Bpanel4ProductRatingsServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-product-ratings';

    public function boot(
        BladeCompiler $blade,
        LivewireManager $livewire,
    ): void {
        $this->commands([InstallCommand::class]);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $blade->component(ProductRatingsSummary::class, self::PACKAGE_PREFIX . '-summary');
        $blade->component(SingleRating::class, self::PACKAGE_PREFIX . '-single');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $livewire->component(self::PACKAGE_PREFIX . '::product-ratings-list', ProductRatingsList::class);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);
        $livewire->component(self::PACKAGE_PREFIX . '::livewire.ratings-table', ProductRatingsDatatable::class);
    }
}
