<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\Http\Livewire;

use Bittacora\Bpanel4\ProductRatings\Models\ProductRating;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Livewire\WithPagination;
use RuntimeException;

final class ProductRatingsList extends Component
{
    use WithPagination;

    protected string $paginationTheme = 'bootstrap';

    private ?Factory $view = null;

    public ?Product $product = null;

    public function boot(Factory $view): void
    {
        $this->view = $view;
    }

    public function render(): View
    {
        if (!$this->view instanceof Factory) {
            throw new RuntimeException();
        }

        if (!$this->product instanceof Product) {
            throw new RuntimeException();
        }

        return $this->view->make('bpanel4-product-ratings::livewire.list', [
            'ratings' => ProductRating::with(['client:id,name,surname'])->where('product_id', $this->product->getId())
                         ->where('active', true)->orderBy('created_at', 'DESC')->paginate(3),
        ]);
    }
}
