<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\Http\Livewire;

use Bittacora\Bpanel4\ProductRatings\Models\ProductRating;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

final class ProductRatingsDatatable extends DataTableComponent
{
    /**
     * @var array<int>
     * @phpstan-ignore-next-line no le pongo un valor por defecto porque falla
     */
    public array $selectedKeys;

    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make('Cliente', 'name')->addClass("w-30 "),
            Column::make('Producto', 'product')->addClass("w-30 "),
            Column::make('Valoración', 'rating')->addClass("w-5 "),
            Column::make('Título', 'title')->addClass("w-30 "),
            Column::make('Mensaje', 'message')->addClass("w-30 "),
            Column::make('Mostrada')->addClass("w-30 "),
            Column::make('Acciones')->addClass("w-30 "),
        ];
    }

    /**
     * @return Builder<ProductRating>
     */
    public function query(): Builder
    {
        return ProductRating::query()
            ->with(['client', 'product'])
            ->orderBy('id', 'DESC')
            ->when(
                $this->getFilter('search'),
                fn (Builder $query, string|int|null $term): Builder => $query
                    ->where('title', 'like', '%' . $term . '%')
                    ->orWhere('message', 'like', '%' . $term . '%')
            );
    }

    public function rowView(): string
    {
        return 'bpanel4-product-ratings::bpanel.livewire.product-datatable';
    }

    /**
     * @return array{bulkDelete: string}
     */
    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar',
        ];
    }

    public function bulkDelete(): void
    {
        if ([] !== $this->selectedKeys()) {
            ProductRating::destroy($this->selectedKeys);
            $this->resetAll();
        }
    }
}
