<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\Http\Controllers;

use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Clients\Services\ClientService;
use Bittacora\Bpanel4\ProductRatings\Actions\StoreRating;
use Bittacora\Bpanel4\ProductRatings\Requests\StoreRatingRequest;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Database\DatabaseManager;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Throwable;

final class ProductRatingController
{
    public function __construct(
        private readonly Redirector $redirector,
        private readonly ClientService $clientService,
        private readonly DatabaseManager $db,
        private readonly ExceptionHandler $exceptionHandler,
    ) {
    }

    /**
     * @throws UserNotLoggedInException
     * @throws Throwable
     */
    public function store(StoreRatingRequest $request, StoreRating $storeRating): RedirectResponse
    {
        $this->db->beginTransaction();
        try {
            $data = $request->validated();
            $data['client_id'] = $this->clientService->getCurrentClient()->getClientId();
            $storeRating->execute($data);
            $this->db->commit();

            return $this->redirector->back()->with([
                'alert-success' => '¡Gracias por enviar su opinión! La revisaremos y la publicaremos lo antes posible.',
            ]);
        } catch (Throwable $e) {
            $this->exceptionHandler->report($e);
            $this->db->rollBack();
            return $this->redirector->back()->with(['alert-danger' => 'Ocurrió un error al publicar su opinión.']);
        }
    }
}
