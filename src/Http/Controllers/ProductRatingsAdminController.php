<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\Http\Controllers;

use Bittacora\Bpanel4\ProductRatings\Actions\UpdateRating;
use Bittacora\Bpanel4\ProductRatings\Dto\RatingDto;
use Bittacora\Bpanel4\ProductRatings\Http\Requests\UpdateRatingRequest;
use Bittacora\Bpanel4\ProductRatings\Models\ProductRating;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Translation\Translator;
use Illuminate\View\Factory;
use Throwable;

final class ProductRatingsAdminController
{
    public function __construct(
        private readonly Factory $view,
        private readonly UrlGenerator $urlGenerator,
        private readonly Redirector $redirector,
        private readonly ExceptionHandler $exceptionHandler,
        private readonly Translator $translator,
    ) {
    }

    public function index(): View
    {
        return $this->view->make('bpanel4-product-ratings::bpanel.index');
    }

    public function edit(ProductRating $rating): View
    {
        return $this->view->make('bpanel4-product-ratings::bpanel.edit', [
            'action' => $this->urlGenerator->route('bpanel4-product-ratings.update', ['rating' => $rating]),
            'rating' => $rating,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function update(UpdateRatingRequest $request, UpdateRating $updateRating): RedirectResponse
    {
        try {
            $dto = RatingDto::fromArray($request->validated());
            $updateRating->execute($dto);
            return $this->redirector->route('bpanel4-product-ratings.index')->with([
                'alert-success' => $this->translator->get('bpanel4-product-ratings::general.rating-updated'),
            ]);
        } catch (Throwable $e) {
            $this->exceptionHandler->report($e);
            return $this->redirector->back()->with([
                'alert-danger' => $this->translator->get('bpanel4-product-ratings::general.rating-not-updated'),
            ]);
        }
    }
}
