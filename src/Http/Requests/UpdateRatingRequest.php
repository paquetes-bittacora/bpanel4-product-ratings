<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @method array validated($key = null, $default = null)
 */
final class UpdateRatingRequest extends FormRequest
{
    public function authorize(): bool
    {
        $user = $this->user();

        if (null === $user) {
            return false;
        }

        return $user->can('bpanel4-product-ratings.update');
    }

    /**
     * @return array<string, string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:255',
            'message' => 'required|string',
            'active' => 'nullable|boolean',
            'id' => 'exists:products_ratings,id',
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge(['active' => $this->get('active') ?? false]);
    }
}
