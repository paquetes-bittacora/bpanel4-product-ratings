<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\Tests\Unit\Actions;

use Bittacora\Bpanel4\ProductRatings\Actions\UpdateRating;
use Bittacora\Bpanel4\ProductRatings\Database\Factories\ProductRatingFactory;
use Bittacora\Bpanel4\ProductRatings\Dto\RatingDto;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class UpdateRatingTest extends TestCase
{
    use RefreshDatabase;

    private UpdateRating $updateRating;

    protected function setUp(): void
    {
        parent::setUp();
        $this->updateRating = $this->app->make(UpdateRating::class);
    }

    public function testActualizaUnaValoracion(): void
    {
        $rating = (new ProductRatingFactory())->createOne();
        $dto = new RatingDto(
            id: $rating->getId(),
            title: 'Título modificado',
            message: 'Mensaje modificado',
            active: true
        );
        $this->updateRating->execute($dto);

        $rating->refresh();
        $this->assertEquals('Título modificado', $rating->getTitle());
        $this->assertEquals('Mensaje modificado', $rating->getMessage());
    }
}
