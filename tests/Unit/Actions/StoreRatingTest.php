<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductRatings\Tests\Unit\Actions;

use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\ProductRatings\Actions\StoreRating;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class StoreRatingTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private StoreRating $storeRating;

    protected function setUp(): void
    {
        parent::setUp();
        $this->storeRating = $this->app->make(StoreRating::class);
    }

    public function testCreaUnaOpinion(): void
    {
        $clientId = (new ClientFactory())->createOne()->getClientId();
        $productId = (new ProductFactory())->createOne()->getId();

        $this->storeRating->execute([
            'title' => $this->faker->text,
            'text' => $this->faker->text,
            'rating' => $this->faker->randomFloat(max: 5),
            'client_id' => $clientId,
            'product_id' => $productId,
        ]);

        $this->assertDatabaseHas('products_ratings', ['client_id' => $clientId, 'product_id' => $productId]);
    }
}
